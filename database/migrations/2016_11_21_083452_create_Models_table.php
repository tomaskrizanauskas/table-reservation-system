<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
         });
     
        Schema::create('dishes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('photo')->nullable();
            $table->text('description');
            $table->decimal('netto_price', 8, 2);
            $table->decimal('price', 8, 2);
            $table->integer('quantity');
            $table->integer('menu_id')->unsigned();
            $table->timestamps();

            $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
            
        });

        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('number_of_persons');
            $table->timestamps();
        });

         Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->float('longitude', 8, 3);
            $table->float('latitude', 8, 3);
            $table->string('name');
            $table->text('working_hours');
            $table->text('info');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('table_id')->unsigned();
            $table->integer('number_of_persons');
            $table->string('contact_phone');
            $table->datetime('reserved_at');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('table_id')->references('id')->on('tables')->onDelete('cascade');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->decimal('netto_price',8, 2);
            $table->decimal('price',8 ,2);
            $table->integer('quantity');
            $table->string('phone');
            $table->integer('order_id')->unsigned();
            $table->integer('dish_id')->unsigned();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('dish_id')->references('id')->on('dishes')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('dishes');
        Schema::dropIfExists('tables');
        Schema::dropIfExists('carts');
        Schema::dropIfExists('contacts');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('menus');
    }
}
