<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'remember_token' => str_random(10),
        'phone_number' => $faker->e164PhoneNumber(),
            'city' => $faker->city(),
            'zip_code' => $faker->postcode(),
            'adress' => $faker->address(),
         	'birthdate' => rand(1955,2000).'-'.str_pad(rand(1,12), 2, '0', STR_PAD_LEFT).'-'.str_pad(rand(1,28), 2, '0', STR_PAD_LEFT),
            'password' => bcrypt('secret'),
            'country' => $faker->country()
    ];
});
  
            