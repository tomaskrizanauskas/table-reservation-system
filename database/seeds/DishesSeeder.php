<?php

use Illuminate\Database\Seeder;

class DishesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dishes')->insert([
            'name' => 'vienas',
            'photo' => 'adresas',
            'description' => 'zjbs skonis',
            'netto_price' => '5',
            'price' => '16',
            'quantity' => 25,
            'menu_id' => 2
        ]);

         DB::table('dishes')->insert([
            'name' => 'antras',
            'photo' => 'adresas',
            'description' => 'zjbs skonis',
            'netto_price' => '29',
            'price' => '11',
            'quantity' => 25,
            'menu_id' => 2
        ]);

          DB::table('dishes')->insert([
            'name' => 'trecias',
            'photo' => 'adresas',
            'description' => 'zjbs skonis',
            'netto_price' => '8',
            'price' => '15',
            'quantity' => 25,
            'menu_id' => 2
        ]);

           DB::table('dishes')->insert([
            'name' => 'ketvirtas',
            'photo' => 'adresas',
            'description' => 'zjbs skonis',
            'netto_price' => '55',
            'price' => '22',
            'quantity' => 25,
            'menu_id' => 2
        ]);

            DB::table('dishes')->insert([
            'name' => 'penktas',
            'photo' => 'adresas',
            'description' => 'zjbs skonis',
            'netto_price' => '10',
            'price' => '44',
            'quantity' => 25,
            'menu_id' => 2
        ]);
    }
}
  