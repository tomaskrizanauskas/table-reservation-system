<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    DB::table('menus')->delete();

    DB::table('menus')->insert([
            'title' => 'Šaltieji užkandžiai',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Sriubos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Karštieji patiekalai',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Desertai',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Salotos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Alkoholiniai gėrimai',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    DB::table('menus')->insert([
            'title' => 'Nealkoholiniai gėrimai',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
