<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\User::class, 20)->create()->each(function($u) {
    $u->issues()->save(factory(App\Issues::class)->make());
  });
    }
}

