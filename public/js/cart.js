$(document).ready(function(){

	$("#cart").on("click", function() {
		$(".shopping-cart").fadeToggle( "fast");
	});
	
	$(document).on('click','.addtocart', function(){
		var id = $(this).data('dish-id')


		$.ajax({
			method: 'POST',
			data: {
				dish_id: id,
				_token: TOKEN
			},
			url: CART_UPDATE_URL,
			success: function(response){
				var itemm = response.total_items>1?' items' : ' item';
				$('.cart-number').html(response.total_items);
				$('.cart-total-amount').html(response.total_amount);

				$('.cart-title').html('<h4>Shopping Cart</h4><p class="item_count">You have '+ response.total_items + itemm 
					+'</p>' + 
					'<a class="btn btn-default pull-left btn-xs cart-destroy-all" href="#">Clear cart</a>');

				$('.cart-items').html(' ');

				for (var i = 0; i<response.items.length; i++){

					var item = '<div class="cart-item clearfix">' +
					'<div class="cart-item-image">' +
					'<a href="#"'+'"><img src="'
					+response.items[i].photo_url
					+'" width="50px" height="50px" alt="'+
					response.items[i].name
					+'"></a>'+'</div>'+
					'<div class="cart-item-desc">'+
					'<a href="#">'+response.items[i].name+'</a>'+
					'<span class="cart-item-price">'+response.items[i].price+'</span>'+
					'<span class="cart-item-quantity">x'+response.items[i].cart_count+'</span>'+
					'<i class="fa fa-times ci-close js-remove" data-item="' + response.items[i].id + '"></i>'+
					'</div></div>';


					$('.cart-items').append(item);

				}
				$('.cart-action-frame').html( 
					'<div class="cart-action clearfix">'
					+'<span class="pull-left checkout-price">'+ response.total_amount +'€</span>'
					+'<a class="btn btn-default pull-right" href="'+ CART_URL +'">View Cart</a>'
					+'</div>'
					);


			}
		});

		return false;
	});


	$(document).on('click', '.cart-destroy-all', function() {
		console.log("pressed");
		$.ajax({
			method: "POST",
			url: CART_DESTROY_ALL_URL,
			data: {
				_token: TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				$('.cart-number').html(0);
				// $('.cart-content').html("");

				$('.cart-content').html('<div class="cart-title clearfix">'
					+'<h4>Shopping Cart</h4>'
					+'<p>Your shopping cart is empty</p>'
					+'</div>'
					+'<div class="cart-items"></div>'
					+'<div class="cart-action-frame"></div>');

				$('.table').html(
						'<tbody>'+
							'<tr>'+
								'<td>'+
									'<h4>Shopping Cart</h4>'+
									'<p>Your shopping cart is empty</p>'+
								'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>'+
									'<a href="'+ INDEX_URL +'" class="btn btn-default"><span class="fa fa-shopping-cart"></span> Continue Shopping</a>'+
								'</td>'+
							'</tr>'+
						'</tbody>');
				
			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}
		});
	});

		$(document).on('click', '.js-remove', function() {
		
			var btn = $(this);
			var item = $(this).data('item')

		$.ajax({
			method: "POST",
			url: CART_DESTROY_ITEM_URL,
			data: {
				_token: TOKEN,
				item: item

			},
			success: function(response) {
				
				$('.js-remove').each(function() {
   				 if($( this ).data('item') == item){
   				 	$( this ).parent().parent().remove();
   				 }
  				});
				
				//cart
				var items = response.total_items>1?' items':' item';

				$('.cart-number').html(response.total_items);
				$('.cart-total-amount').html(response.total_amount);
				$('.item_count').html('You have ' + response.total_items + items);
				$('.checkout-price').html(response.total_amount + ' €');

				if(response.total_items < 1){
					$('.cart-action-frame').html(' ');
					$('.cart-title').html('<h4>Shopping Cart</h4><p>Your shopping cart is empty</p>');
					
				}

				//checkout

				$('.total-checkout').html('<h3>' + response.total_amount +' €</h3>');

				if(response.total_items < 1){
					$('.table').html(
						'<tbody>'+
							'<tr>'+
								'<td>'+
									'<h4>Shopping Cart</h4>'+
									'<p>Your shopping cart is empty</p>'+
								'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>'+
									'<a href="'+ INDEX_URL +'" class="btn btn-default"><span class="fa fa-shopping-cart"></span> Continue Shopping</a>'+
								'</td>'+
							'</tr>'+
						'</tbody>');
				}

			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}
		});
	});

});