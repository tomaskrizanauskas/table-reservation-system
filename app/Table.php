<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{

	 protected $fillable = [
        'number_of_persons', 'name'
    ];
       public function orders()
    {
        return $this->hasMany('App\Order');
    }


    function getNameWithCapacityAttribute()
    {
        return $this->name . ' (' . $this->number_of_persons . ' persons)';
    }
}
