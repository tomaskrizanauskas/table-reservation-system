<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{


    protected $fillable = ["table_id", "number_of_persons", "contact_phone", "reserved_at", "reservation_date",];

     public function user()
    {
        return $this->belongsTo('App\User');
    }


      public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

       public function table()
    {
        return $this->belongsTo('App\Table');
    }
}
