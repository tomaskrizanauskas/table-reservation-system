<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

 class Dish extends Model
 {
    protected $fillable = [
    'name',
    'photo',
    'description',
    'netto_price',
    'price',
    'quantity',
    'menu_id'];



    public function carts()
    {
        return $this->belongsToMany('App\Cart');
    }

        public function orders()
    {
        return $this->hasManyThrough('App\Order', 'App\OrderItem');
    }

      public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

 }
