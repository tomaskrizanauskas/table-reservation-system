<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DishesMenuController extends Controller
{
    public function __construct(){
        $this->middleware('admin', ['except' => ['index','show']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $id = null)
    {   

        $menu_id = $id->id;
        $menus = \App\Menu::all();
        

        $selectMenus = [];

        foreach ($menus as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }
        return view('menu.createDish',compact('selectMenus','menu_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

       
        if ($request->hasFile('photo') && substr($request->file('photo')->getMimeType(), 0, 5) == 'image') {
    
        $photo = $request->file('photo');

        $img = \Storage::disk('public')->put('photos', $photo);
            $photo = true;

        }else {
            $photo = false;
        }

         $dish = \App\Dish::create(array_merge($request->all(), ['photo' => $img]));
        // $destinationPath = base_path() . '/public/uploads/dishes/';
        // $photo->move($destinationPath, $photo->getClientOriginalName());


         ////////////////xujovai

        return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dish = \App\Dish::find($id);
        return view('menu.dishShow', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dish = \App\Dish::find($id);
        $menus = \App\Menu::all();

        $selectMenus = [];

        foreach ($menus as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }
        return view('menu.createDish', compact('dish', 'selectMenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->hasFile('photo') && substr($request->file('photo')->getMimeType(), 0, 5) == 'image') {
    
        $photo = $request->file('photo');

        $img = \Storage::disk('public')->put('photos', $photo);
            $save = array_merge($request->all(), ['photo' => $img]);

        }else {
            $save = $request->all();
        }


        $dish = \App\Dish::find($id)->update($save);
        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $menu_id = \App\Dish::find($id)->menu_id;

         \App\Dish::find($id)->delete();

        return redirect()->route('menu.show',$menu_id);
    }
}
