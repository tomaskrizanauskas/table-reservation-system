<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Table;
use Carbon\Carbon;

class OrderController extends Controller
{

    public function __construct(){
        $this->middleware('cart.empty')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $orders = Auth::user()->orders;
        //where user_id =dabartinis

        return view('orders.orderedIndex', compact('user', 'orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user   = Auth::user();
        $tables = Table::all();
        $tables = $tables->pluck('nameWithCapacity', 'id');
        return view('orders.orderCreate', compact('user', 'tables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
        'table_id' => 'required|exists:tables,id|integer',
        'contact_phone' => 'required|min:8|max:12',
        'number_of_persons' => 'required|min:1|max:100|integer',
        'reserved_at' => 'required|date_format:Y-m-d H:i'
    ]);


        $capacity = Table::find($request->table_id)->number_of_persons;
        $number_of_persons=$request->number_of_persons;
        if($capacity < $number_of_persons){
            return redirect()
            ->back()
            ->withErrors('Zmones netelpa')
            ->withInput();
        }
        $before = new Carbon($request->reserved_at);
        $before = $before->subHours(1);

        $after = new Carbon($request->reserved_at);

        $after = $after->addHours(1);

        

        $orders = Order::whereBetween('reserved_at' , [$before, $after])->where('table_id', $request->table_id
            )->get();

        if(count($orders)){
            return back()
            ->withErrors('Table is reserved')
            ->withInput();
        }

        $data = $request->all();

        $user = Auth::user();

        $order = $user->orders()->create($data);

        $cartItems=session()->get('cart.items');

        foreach ($cartItems as $item) {

            $item['dish_id'] = $item['id'];
            $item['title'] = $item['name'];
            $order->orderItems()->create($item);
        }

        $request->session()->forget('cart.items');
        $request->session()->forget('cart.total_items');
        $request->session()->forget('cart.total_amount');


        //nebaigta

        return redirect()->route('order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        return view('orders.success', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
