<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{

    public function __construct(){
        $this->middleware('cart.empty')->except('store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $items = session()->get('cart.items');
        return view('orders.checkout', compact('items'));
    }


    public function store(Request $request)
    {     
        $total_items = 0;
        $total_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;


        $dish = \App\Dish::find($id)->toArray();
        $dish['cart_count'] = 1;
        $dish['photo_url'] = url($dish['photo']);
        $dish['route'] = route('dishes.show', $dish['id']);

        $items = $request->session()->get('cart.items');

    

        if($items) {

            $found = false;
            for($i = 0; $i < count($items); $i++){
                if($items[$i]['id'] == $dish['id']){
                    $items[$i]['cart_count']++;
                    $found = true;
                    break;
                }
            }

            if(!$found){
                $items[] = $dish;
            }

            for($i = 0; $i < count($items); $i++){
                $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
                $total_items += $items[$i]['cart_count'];
            }

        } else {
            $items[] = $dish;
            $total_amount = $dish['price'];
            $total_items = 1;
        }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];



        return $result;
    }

   
    public function destroyAll(Request $request)
    {
        // $request->session()->flush();

        $request->session()->forget('cart.items');
        $request->session()->forget('cart.total_items');
        $request->session()->forget('cart.total_amount');


    }

     public function destroyOne(Request $request)
    {

        $id = $request->item;
        $total_items = 0;
        $total_amount = 0;

        $items = $request->session()->get('cart.items');

        // dd([$items, $id] );

        foreach($items as $i => $item){


            if($item['id'] == $id){ 
                array_splice($items, $i, 1);
                break;
            }
        }

        for($i = 0; $i < count($items); $i++){
                $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
                $total_items += $items[$i]['cart_count'];
            }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);


// Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }

}