<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $cart = Session::get('cart.total_items');

        if(count($cart)){
            return $next($request);
        }else{
            return redirect()->route('index');

            // echo "<h1>nifiga";
            // echo $cart;
            // echo "</h1>";

        }
    }
}

