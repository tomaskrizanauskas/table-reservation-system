<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'surname', 'birthdate', 'phone_number', 'city', 'zip_code', 'adress', 'country','password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function cart()
    {
        return $this->hasOne('App\Cart');
    }

     public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function isAdmin()
    {
        return $this->admin; // this looks for an admin column in your users table
    }
}
