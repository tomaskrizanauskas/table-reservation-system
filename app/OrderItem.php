<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	protected $fillable = ["title","description","netto_price","price","quantity","phone", "order_id", "dish_id","phone"];

       public function order()
    {
        return $this->belongsTo('App\Order');
    }

       public function dish()
    {
        return $this->hasOne('App\Dish');
    }
}
