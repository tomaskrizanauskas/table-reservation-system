@extends("layouts.app")
@section("content")

<div class="container">
    
    <div class="row">

<h3>{{ isset($dish) ? 'Edit Dish' : 'Add a new dish' }}</h3>

    @if(isset($dish))
        {{ Form::open(['route' => ["dishes.update", $dish->id], 'method' => 'POST', 'files' => true] ) }}
    {{ Form::hidden('_method', 'PUT') }}
        
    @else
        {{ Form::open(['route' => ["dishes.store"], 'method' => 'POST', 'files' => true]) }}
    @endif

    {{ Form::label('title', 'Patiekalo pavadinimas:')}}
    {{ Form::text('name', isset($dish->name)?$dish->name:'', ['class'=>'form-control']) }}

    {{ Form::label('title', 'Paveiksliukas:')}}
    {{ Form::file('photo', ['class'=>'form-control']) }}

    {{ Form::label('netto', 'Netto kaina:')}}
    {{ Form::text('netto_price', isset($dish->netto_price)?$dish->netto_price:'', ['class'=>'form-control']) }}

    {{ Form::label('price', 'Patiekalo mažmeninė kaina:')}}
    {{ Form::text('price', isset($dish->price)?$dish->price:'', ['class'=>'form-control']) }}

    {{ Form::label('kiekis', 'Kiekis:')}}
    {{ Form::text('quantity', isset($dish->quantity)?$dish->quantity:'', ['class'=>'form-control']) }}

    {{ Form::label('tipas', 'Tipas:')}}

    
    {{Form::select('menu_id', $selectMenus, isset($dish->menu_id)?$dish->menu_id:$menu_id, ['class'=>'form-control']) }}
    


    {{ Form::label('description', 'Aprašymas:')}}
    {{ Form::textarea('description',isset($dish->description)?$dish->description:'', ['class'=>'form-control', 'rows' => '10', 'cols' => '30']) }}

    {{ form::submit(isset($dish) ? 'Edit Dish' : 'Add new dish', ['class'=>'btn btn-primary']) }}
    {{ Form::close() }}

    @if(isset($dish))
        {{ Form::open(['route' => ['dishes.destroy', $dish->id], 'method' => 'POST']) }}

        {{ Form::hidden('_method', 'DELETE') }}
        {{ form::submit('Delete', ['class'=>'btn btn-danger']) }}

        {{ Form::close() }}
    @endif

    </div>
    </div>
@endsection