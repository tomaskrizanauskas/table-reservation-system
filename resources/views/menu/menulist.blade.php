@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
	@foreach($menu as $index => $dish)
    
        <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="dish">
                <div class="dish-img">
                    <a href="{{ route('dishes.show', $dish->id)}}"><img class="img img-responsive" src="{{ url($dish->photo) }}"></a>
                </div>
                <div class="dish-info">
                    <h4><a class="title" href="{{ route('dishes.show', $dish->id) }}">{{ $dish->name }}</a></h4>
                    <p class ="justify"><i>{{ mb_strimwidth($dish->description, 0, 50, '...') }}</i></p>
                    <p>Kaina: {{ $dish->price }} €</p>
                    @if(auth::check() && auth::user()->is_admin)
                    <a class="btn btn-primary" href="{{ route('dishes.edit',$dish->id) }}">Edit</a>
                    @endif
                    <a class="addtocart btn btn-default" href="#" data-dish-id="{{$dish->id}}">ADD to Chart</a>
                </div>
            </div>
        </div>
        @if(++$index % 3 == 0)
            </div><div class="row">
        @endif
    @endforeach
	</div>
</div>

@endsection