@extends("layouts.app")
@section("content")

<div class="container">
    
    <div class="row">

<h3>{{ isset($menu) ? 'Edit Menu' : 'Add a new menu' }}</h3>

    @if(isset($menu))
        {{ Form::open(['route' => ["menu.update", $menu->id], 'method' => 'POST'] ) }}
    {{ Form::hidden('_method', 'PUT') }}
        
    @else
        {{ Form::open(['route' => ["menu.store"], 'method' => 'POST']) }}
    @endif

    {{ Form::label('title', 'Patiekalo pavadinimas:')}}
    {{ Form::text('title', isset($menu->title)?$menu->title:'', ['class'=>'form-control']) }}

    {{ form::submit(isset($menu) ? 'Edit Menu' : 'Add new menu', ['class'=>'btn btn-primary']) }}
    {{ Form::close() }}

    @if(isset($menu))
        {{ Form::open(['route' => ['menu.destroy', $menu->id], 'method' => 'POST']) }}

        {{ Form::hidden('_method', 'DELETE') }}
        {{ form::submit('Delete', ['class'=>'btn btn-danger']) }}

        {{ Form::close() }}
    @endif

    </div>
    </div>
@endsection