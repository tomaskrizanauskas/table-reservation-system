@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
	
		<h3><a href="{{ route('dishes.show', $dish->id) }}">{{ $dish->name }}</a></h5>
		<a href=""><img src="{{ $dish->photo }}"></a>
		<p><i>{{ $dish->description }}</i></p>
		<p>Kaina: {{ $dish->price }} €</p>

	</div>
</div>

@endsection