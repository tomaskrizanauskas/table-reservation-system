@extends('layouts.app')

@section('content')
<div class="container">
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<div class="row">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					Reservarion Details
				</h3>
			</div>
			<div class="panel-body">
				<form role="form" method="POST" action="{{ route('order.store') }}">

					<div class="row">

						{{--{{ Form::open(['url' => route('order.store')]) }}--}}

						{{ csrf_field() }}
						<div class="row">
							<div class="form-group">
								<label for="table_id" class="col-md-2 col-md-offset-2 control-label">Table</label>
								<div class="col-md-8">
			                        <select name="table_id" id="table_id">
			                         @foreach($tables as $id => $name)
			                         	<option value="{{$id}}" {{ old('table_id')==$id?'selected':'' }}>{{$name}}</option>
			                         @endforeach
			                     	</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<label for="number_of_persons" class="col-md-2 col-md-offset-2 control-label">Number Of Persons</label>
								<div class="col-md-8">
									<input type="number" name="number_of_persons" value="{{old('number_of_persons', '2') }}" min="1">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<label for="contact_phone" class="col-md-2 col-md-offset-2 control-label">Phone</label>
								<div class="col-md-8">
									{{--            {{ Form::text('contact_phone', '+370') }}--}}

									<input type="text" name="contact_phone" value="{{old('contact_phone' , '+370')}}">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<label for="reserved_at" class="col-md-2 col-md-offset-2 control-label">Reservation date and time</label>
								<div class="col-md-8">
									<input type="text" name="reserved_at" value="{{old('reserved_at')}}">	
									<p class="help-block">2016-12-24 16:00</p>
								</div>
							</div>
						</div>


						<div class="row">
							<div class="form-group">
								<div class="col-md-6 col-md-offset-2">
									<button class="btn btn-success" type="submit" name="submit">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection