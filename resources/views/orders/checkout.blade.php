@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-10 col-md-offset-1">
				<table class="table table-hover">
				@if(Session::get('cart.total_items') > 0)
					<thead>
						<tr>
							<th>Product</th>
							<th>Quantity</th>
							<th class="text-center">Price</th>
							<th class="text-center">Total</th>
							<th> </th>
						</tr>
					</thead>
					<tbody>
						@foreach($items as $item)
						
						<tr>
							<td class="col-sm-8 col-md-6">
							<div class="media">
								<a class="thumbnail pull-left" href="#"> <img class="media-object" src="{{ url($item['photo']) }}" style="width: 72px; height: 72px;"> </a>
								<div class="media-body">
									<h4 class="media-heading"><a href="#">{{ $item['name'] }}</a></h4>
									<h5 class="media-heading"> by <a href="#">{{ $item['description'] }}</a></h5>
									<span>Status: </span><span class="text-success"><strong>In Stock</strong></span>
								</div>
							</div></td>
							<td class="col-sm-1 col-md-1" style="text-align: center">
							<input type="number" class="form-control" id="" value="{{ $item['cart_count'] }}">
							</td>
							<td class="col-sm-1 col-md-1 text-center"><strong>{{ $item['price'] }}€</strong></td>
							<td class="col-sm-1 col-md-1 text-center"><strong>{{ number_format($item['price'] * $item['cart_count'], 2, '.', '') }} €</strong></td>
							<td class="col-sm-1 col-md-1">
							<button type="button" class="btn btn-danger js-remove" data-item="{{ $item['id']}}">
								<span class="fa fa-remove "></span> Remove
							</button></td>
						</tr>
						@endforeach

					</tbody>
					<tfoot>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td><h3>Total</h3></td>
							<td class="text-right total-checkout"><h3>{{ Session::get('cart.total_amount') }} €</h3></td>
						</tr>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td>
							<a href="{{route('index')}}" class="btn btn-default">
								<span class="fa fa-shopping-cart"></span> Continue Shopping
							</a></td>
							<td>
							<a class="btn btn-success" href="{{ route('order.create') }}">
								Checkout <span class="fa fa-play"></span>
							</a></td>
						</tr>
					</tfoot>
					@else
					<tr>
						<td>
							<h4>Shopping Cart</h4>
							<p>Your shopping cart is empty</p>
						</td>
					</tr>
					<tr>
						<td>
							<a href="{{ route('index') }}" class="btn btn-default"><span class="fa fa-shopping-cart"></span> Continue Shopping</a>
						</td>
					</tr>
					@endif
				</table>
			</div>
		</div>
	</div>

@endsection