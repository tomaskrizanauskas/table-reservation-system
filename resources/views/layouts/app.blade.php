<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Restaurant - @yield('title')</title>

	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<script>
		var TOKEN = "{{ csrf_token() }}";
		var CART_UPDATE_URL = "{{route('cart.store')}}";
		var CART_DESTROY_ALL_URL = "{{route('cart.destroyAll')}}";
		var CART_DESTROY_ITEM_URL = "{{route('cart.destroyOne')}}";
        var CART_URL = "{{route('checkout')}}";
        var INDEX_URL = "{{route('index')}}";
        var CHECKOUT_URL = "{{route('order.create')}}";
	</script>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- <link rel="icon" href="../../favicon.ico"> -->

	<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"> -->

	<!-- Custom styles for this template -->
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugin.css') }}" rel="stylesheet">
	<link href="{{ asset('css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('css/fixes.css') }}" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



	<body>


		<nav class="navbar navbar-fixed-top" style="background: rgba(0, 0, 0, 0.508039);">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{ route('index') }}">
						<img src="{{ url('img/nav-logo.png') }}" alt="nav-logo">
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="{{ route('index') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
							<ul class="dropdown-menu">
								@foreach (\App\Menu::all() as $menu)
								<li><a href="{{ route('menu.show', $menu->id) }}">{{ $menu->title }}</a></li>
								@endforeach
								
								@if(auth::check() && auth::user()->is_admin)
								<li><a href="{{ route('menu.create') }}">+ Sukurti meniu + </a></li>
								@endif
							</ul>
						</li>


						<li><a href="{{ url('/contacts') }}">Contact</a></li>



						<li class="dropdown">
							<a class="css-pointer dropdown-toggle clearfix" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart fsc"></i><span class="cart-number">{{  Session::get('cart.total_items')?Session::get('cart.total_items'):'0' }}</span><span class="caret up"></span></a>

							@if(Session::get('cart.total_items') > 0)
							<div class="cart-content dropdown-menu">
								
								<div class="cart-title clearfix">
									
									<h4>Shopping Cart</h4>
									<p class='item_count'>You have {{ Session::get('cart.total_items')}} {{ Session::get('cart.total_items')>1? 'items' : 'item' }} </p>
									
									<a class="btn btn-default pull-left btn-xs cart-destroy-all" href="#">Clear cart</a>
								</div>
								<div class="cart-items">
									@foreach(Session::get('cart.items') as $dish)

									<div class="cart-item clearfix">
										<div class="cart-item-image">
											<a href="{{ route('dishes.show', $dish['id']) }}"><img src="{{ url($dish['photo']) }}" width="50px" height="50px" alt="{{$dish['name'] }}"></a>
										</div>
										<div class="cart-item-desc">
											<a href="{{ route('dishes.show', $dish['id']) }}">{{$dish['name'] }}</a>
											<span class="cart-item-price">{{$dish['price'] }}</span>
											<span class="cart-item-quantity">x {{$dish['cart_count']}}</span>
											<i class="fa fa-times ci-close js-remove" data-item="{{ $dish['id']}}"></i>
										</div>
									</div>

									@endforeach
								</div>
								<div class="cart-action-frame">
									<div class="cart-action clearfix">
										<span class="pull-left checkout-price">{{ Session::get('cart.total_amount') }} €</span>
										<a class="btn btn-default pull-right" href="{{route('checkout')}}">View Cart</a>
									</div>
								</div>

							</div>
							@else
							<div class="cart-content dropdown-menu">
								<div class="cart-title clearfix">
									
									<h4>Shopping Cart</h4>
									<p>Your shopping cart is empty</p>
									
								</div>
								<div class="cart-items">
								</div>

								<div class="cart-action-frame">
								</div>
							</div>
							@endif
						</li>

						@if(Auth::user())
						<li>
							<a href="{{ url('/logout') }}"
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							Logout
						</a>

						<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
						</li>
						<li>
						<a href="#">{{ auth::user()->name }} profile</a>
						</li>
						@if(auth::check() && auth::user()->is_admin)
						<li>
						<a href="#">Admin CP</a>
						</li>
						@endif
					@else

					<li>
						<li><a href="{{ url('/register') }}">REGISTER</a></li>
						<li><a href="{{ url('/login') }}">LOGIN</a></li>
					</li>

					@endif

				</ul>
			</div>

		</div>
	</nav>





	@yield('content')

<footer>
  <div class="container">
    <p>Table reservation system 2016</p>
  </div>
</footer>



	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>


	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

</body>
</html>