@extends('layouts.app')

@section('content')

<div class="container">
	@foreach ($menus as $menu)  
    <div class="row space-bottom-5">
                        
        <h3 class="padding-left-20"><a class="red" href="{{ route('menu.show', $menu->id) }}">{{ $menu->title }}</a></h3>
        @if(auth::check() && auth::user()->is_admin)
        <a class="red padding-left-20" href="{{ route('dishes.create')}}?id={{$menu->id}}">Add a new dish</a>
        <a class="red" href ="{{ route('menu.edit', $menu->id) }}">( Edit )</a>
        @endif
    </div>
    <div class="row">
        @foreach($menu->dishes->take(3) as $index => $dish)
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="dish">
                <div class="dish-img">
                    <a href="{{ route('dishes.show', $dish->id)}}"><img class="img img-responsive" src="{{ url($dish->photo) }}"></a>
                </div>
                <div class="dish-info">
                    <h4><a class="title" href="{{ route('dishes.show', $dish->id)}} ">{{ $dish->name }}</a></h4>
                    <p class ="justify"><i>{{ mb_strimwidth($dish->description, 0, 50, '...') }}</i></p>
                    <p>Kaina: {{ $dish->price }} €</p>
                    @if(auth::check() && auth::user()->is_admin)
                    <a class="btn btn-primary" href="{{ route('dishes.edit',$dish->id) }}">Edit</a>
                    @endif
                    <a class="addtocart btn btn-default" href="#" data-dish-id="{{$dish->id}}">ADD to Chart</a>
                </div>
            </div>
        </div>
            
        @endforeach
    </div>
    @endforeach
</div>

@endsection


								