This site (the final work of 3W Academy Vilnius courses) was created using:
- Laravel framework;
- HTML;
- CSS;
- PHP;
- JQUERY;
- MySQL database;

I also used Bootstrap framework and some own CSS code for  mobile responsive.

Has these elements:
    - authorisation for admin 
    - connection with SQL database 
        - users
        - albums
        - photos
    - working CRUD
    - working relationships (data model) as one to many (album contains many photos) and one to one (photo belongs to one album); 
    - showing photos via carousel slider;
    - contact page with maps;
    - responsive design;

- to run project create db table "album" in php my admin
- run command "git clone https://tomaskrizanauskas@bitbucket.org/tomaskrizanauskas/album.git"
- run command "composer install"
- chmod all files and folders inside album 777 "sudo chmod -R 777 ." or equivalent for your operating system
- rename ".env.example" file in main directory to ".env"
- run "php artisan key:generate"
- open ".env" file, change DB_DATABASE=homestead DB_USERNAME=homestead DB_PASSWORD=secret to DB_DATABASE=album DB_USERNAME=root DB_PASSWORD=
- run command "php artisan migrate"

- to make admin run command: "php artisan db:seed --class=AdminSeeder", it will generate admin acount to login use "admin@admin.com" , password "123456".

- Login as admin and explore album features.