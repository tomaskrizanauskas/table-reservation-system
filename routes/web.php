<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', "HomeController@index")->name('index');
Route::get('contacts', function () {
	
	$id = 2;
  $contacts = \App\Contacts::find($id);

  return view('contacts', compact('contacts'));
});

Route::resource('dishes', 'DishesMenuController');
Route::resource('menu', 'MenuController');
Route::post('/cart', 'CartController@store')->name('cart.store');

Route::post('/cartclear', 'CartController@destroyAll')->name('cart.destroyAll');
Route::post('/cartclearone', 'CartController@destroyOne')->name('cart.destroyOne');
Route::get('/cartindex', 'CartController@index')->name('checkout');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('order', 'OrderController');
});

Auth::routes();
